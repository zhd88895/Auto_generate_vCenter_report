import ctypes
import datetime
import fnmatch
import hashlib
import http.client
import json
import logging
import os
import random
import re
import sys
import time
import urllib
import configparser
import shutil

import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt

from docx import Document
from docx.enum.table import WD_CELL_VERTICAL_ALIGNMENT, WD_TABLE_ALIGNMENT
from docx.enum.text import WD_LINE_SPACING, WD_ALIGN_PARAGRAPH
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls
from docx.oxml.shared import OxmlElement, qn
from docx.shared import RGBColor, Cm, Pt
from retry import retry
from win32com import client as ws
from tqdm import tqdm

logging.getLogger("matplotlib").setLevel(logging.WARNING)
logging.getLogger("PIL").setLevel(logging.WARNING)
logging.getLogger("PIL.PngImagePlugin").setLevel(logging.WARNING)

template_flag = False  # 判断是否有模板，有则使用模板生成文件
# json_flag = False  # 判断当前目录下是否有字典，有则使用字典翻译文件内容
rvtools_flag = False  # 判断当前目录下是否有RVTools文件，有则获取RVTools中的数据导入文档中

current_dir = os.getcwd()  # 获取脚本所在目录
d1 = datetime.date.today()  # 获取系统时间
output_docx_name = ''
translate_count = 0

trans_flag = False
log_flag = False
chart_flag = False
page_break_flag = False
template_flag_spec = False
template_filename = ''
customer = ''
appid = ''
secret_key = ''

# 创建一个ConfigParser对象
config = configparser.ConfigParser()
sys.set_int_max_str_digits(65536)

# 定义默认配置参数
######################################
default_config = {
    '是否使用百度翻译功能': 'true',
    '是否生成日志记录': 'true',
    '是否使用内置图表生成功能': 'true',
    '是否在一级标题前插入分页符': 'true',
    '是否使用内置模板': 'true',
    '外置模板文件名': '',
    '关联客户': '',
}
baidufanyi_config = {
    'appid': '',
    'secretkey': '',
}
ziti_config = {
    '段落字体': '等线',
    '标题字体': '等线 Light',
    '表格中文字体': '等线',
    '表格纯英文或数字字体': 'Arial',
}
zihao_config = {
    '中文字号': '11',
    '纯英文或数字字号': '10',
    '1级标题字号': '18',
    '2级标题字号': '14',
}
######################################

# 颜色定义
####################################
title_color = 'D3D3D3'
p1_color = 'FF0000'
p2_color = 'FFA500'
p3_color = 'FFFF00'
ok_color = '009000'
no_color = '7F7F7F'
black_font = RGBColor(0, 0, 0)
white_font = RGBColor(255, 255, 255)
####################################

# 默认字体变量
##################################
default_paragraph_font = ''
default_heading_font = ''
default_table_paragraph_font = ''
default_table_letter_font = ''
default_paragraph_font_size = ''
default_letter_font_size = ''
default_heading_1_font_size = ''
default_heading_2_font_size = ''
##################################

# 内置快速翻译字典
##############################################
replace_dict = {
    'Compute': '计算',
    'Network': '网络',
    'Security': '安全',
    'Storage': '存储',
    'Virtual Machines': '虚拟机',
    'vCenter Server': 'vCenter服务器',
    'vCenterServer': 'vCenter服务器',
    'ESXi Host': 'ESXi主机',
    'Data Center': '数据中心',
    'Datacenter': '数据中心',
    'Licensing': '许可'
}
part_dict = {
    'vCenter Server': 'vCenter服务器',
    'vCenterServer': 'vCenter服务器 ',
    'Server name': '服务器名',
    'System': '系统',
    'Platform': '平台',
    'OS': '操作系统',
    'Number of CPUs': 'CPU数量',
    'RAM': '内存',
    'Cluster': '群集',
    'HBAs': 'HBA卡',
    'NICs': '网卡'
}
net_dict = {
    'Virtual Data Center Name': '数据中心名',
    'Cluster Name': '群集名',
    'ESX/ESXi Hosts': 'ESX/ESXi 主机',
}
trans_dict = {}
##############################################


# 读取配置文件
def read_config():
    global trans_flag, log_flag, chart_flag, page_break_flag, template_flag_spec, customer, appid, \
        secret_key, template_filename, default_paragraph_font, default_heading_font, template_flag, \
        default_table_paragraph_font, default_table_letter_font, default_paragraph_font_size, \
        default_letter_font_size, default_heading_1_font_size, default_heading_2_font_size
    # 检查config文件是否存在，如果不存在，则创建一个初始的config文件
    if not os.path.exists('config.ini'):
        config['功能配置'] = default_config
        config['百度翻译api'] = baidufanyi_config
        config['字体'] = ziti_config
        config['字号'] = zihao_config
        with open('config.ini', 'w') as configfile:
            config.write(configfile)
        print("|", "配置文件已生成，请先查看配置文件中的内容".center(40, "！"), "|")
        print("|", "请不要删除配置文件中的任何一行条目".center(40, "！"), "|")
        print("|", "配置选项填'1'或'true'（忽视大小写）为开启，其它为关闭".center(45, "！"), "|")
        print("|", "请不要删除配置文件中的任何一行条目".center(40, "！"), "|")
        print("|", "非功能开关的值可以自定义，如果留空则使用内置的默认参数".center(40, "！"), "|")
        print("|", "确认后，回车继续".center(40, "！"), "|")
        input()
        print("\033c", end="")
    try:
        # 获取配置参数
        config.read('config.ini')

        trans_flag = config['功能配置']['是否使用百度翻译功能'].lower() == 'true' or config['功能配置'][
            '是否使用百度翻译功能'] == '1'
        log_flag = config['功能配置']['是否生成日志记录'].lower() == 'true' or config['功能配置'][
            '是否生成日志记录'] == '1'
        chart_flag = config['功能配置']['是否使用内置图表生成功能'].lower() == 'true' or config['功能配置'][
            '是否使用内置图表生成功能'] == '1'
        page_break_flag = config['功能配置']['是否在一级标题前插入分页符'].lower() == 'true' or config['功能配置'][
            '是否在一级标题前插入分页符'] == '1'
        template_flag_spec = config['功能配置']['是否使用内置模板'].lower() == 'true' or config['功能配置'][
            '是否使用内置模板'] == '1'
        if trans_flag:
            print("百度翻译功能: 开启")
        else:
            print("百度翻译功能: 关闭")
        if log_flag:
            print("日志记录功能: 开启")
            logging.basicConfig(level=logging.DEBUG,
                                format='%(asctime)s %(name)s:%(levelname)s:%(message)s',
                                datefmt='%Y-%m-%d %H:%M:%S',
                                filename='Run.log',
                                filemode='w')
        else:
            print("日志记录功能: 关闭")
        if chart_flag:
            print("图表生成功能: 开启")
        else:
            print("图表生成功能: 关闭")
        if page_break_flag:
            print("插入分页符: 开启")
        else:
            print("插入分页符: 关闭")
        if template_flag_spec:
            print("使用内置模板: 开启")
            template_flag = True
        else:
            print("使用内置模板: 关闭")
            if template_filename == '':
                template_flag = false
                print("使用外置模板: 关闭")
            else:
                template_filename = config['功能配置']['外置模板文件名']
                template_check(template_filename)
        if config['功能配置']['关联客户'] != '':
            customer = config['功能配置']['关联客户']
        else:
            customer = ''
        if config['百度翻译api']['appid'] != '':
            appid = config['百度翻译api']['appid']
        else:
            appid = ''
        if config['百度翻译api']['secretkey'] != '':
            secret_key = config['百度翻译api']['secretkey']
        else:
            secret_key = ''
        if config['字体']['段落字体'] != '':
            default_paragraph_font = config['字体']['段落字体']
        else:
            default_paragraph_font = '等线'
        if config['字体']['标题字体'] != '':
            default_heading_font = config['字体']['标题字体']
        else:
            default_heading_font = '等线 Light'
        if config['字体']['表格中文字体'] != '':
            default_table_paragraph_font = config['字体']['表格中文字体']
        else:
            default_table_paragraph_font = '等线'
        if config['字体']['表格纯英文或数字字体'] != '':
            default_table_letter_font = config['字体']['表格纯英文或数字字体']
        else:
            default_table_letter_font = 'Arial'
        if config['字号']['中文字号'] != '' or int(config['字号']['中文字号']) > 0:
            default_paragraph_font_size = float(config['字号']['中文字号'])
        else:
            default_paragraph_font_size = 11
        if config['字号']['纯英文或数字字号'] != '' or int(config['字号']['纯英文或数字字号']) > 0:
            default_letter_font_size = float(config['字号']['纯英文或数字字号'])
        else:
            default_letter_font_size = 10
        if config['字号']['1级标题字号'] != '' or int(config['字号']['1级标题字号']) > 0:
            default_heading_1_font_size = float(config['字号']['1级标题字号'])
        else:
            default_heading_1_font_size = 18
        if config['字号']['2级标题字号'] != '' or int(config['字号']['2级标题字号']) > 0:
            default_heading_2_font_size = float(config['字号']['2级标题字号'])
        else:
            default_heading_2_font_size = 14
    except Exception as e:
        print(f"读取配置文件时出现错误{e}")
        print("正在新建配置文件")
        os.unlink(os.path.join(current_dir, 'config.ini'))
        read_config()
    if baidu_fanyi("hello world"):
        trans_flag = True
        print("翻译测试成功")
    else:
        print("翻译测试失败，请检查网络或配置文件中的'appid'及'secretkey'是否可用，已将百度翻译功能关闭")
        trans_flag = False


# 获取超链接显示的文本，舍弃超链接内容
def extract_hyperlink_text(hyperlink_cell):
    # 创建一个空列表，用于存储提取出的超链接文本
    hyperlink_texts = []
    # 遍历单元格中的每个段落
    for paragraph in hyperlink_cell.paragraphs:
        # 遍历段落中的每个文本元素
        for t_para in (re.sub('<[\S\s]*?>', u"", wt) for wt in re.findall('<w:t[\S\s]*?</w:t>', str(paragraph.paragraph_format.element.xml))):
            # 如果文本元素不为空，则去除空格后添加到列表中
            if t_para.strip().replace('\s', ''):
                hyperlink_texts.append(t_para.strip().replace('\s', ''))
    # 将列表中的所有文本元素连接成一个字符串并返回
    return "".join(hyperlink_texts)


# 根据问题编号找到对应的详细问题信息
def find_practice_observation(key_cell, tables):
    for table in tables:
        # flag = False
        # for i in range(len(table.rows)):
        #     for j in range(len(table.rows[i].cells)):
        #         if table.rows[i].cells[j].text == 'Best Practice ID' and extract_hyperlink_text(table.rows[i].cells[j+1]) == key_cell:
        #             flag = True
        #             continue
        #         elif flag and table.rows[i].cells[j].text == 'Observation':
        #             observation = table.rows[i].cells[j+1].text
        #             return observation
        if extract_hyperlink_text(table.rows[1].cells[1]) == key_cell:
            observation = table.rows[3].cells[1].text
            return observation


# 定义一个函数，用于获取外部字典
def dictionary():
    # 设置翻译文件的路径，不再以vSphere版本区分，翻译可共用
    file_path = f"{current_dir}\\translate-dictionary.json"
    # 声明全局变量trans_dict
    global trans_dict
    # 判断文件是否存在
    if os.path.exists(file_path):
        try:
            # 打开文件并读取内容
            with open(file_path, "r", encoding='utf-8') as json_file:
                # 将文件内容加载到trans_dict变量中
                trans_dict = json.load(json_file)
                # 分离文件路径和文件名
                a = os.path.split(file_path)
                # 获取文件名
                b = os.path.splitext(a[-1])[0]
                # 打印已读入字典文件的信息
                print(f"已读入字典文件： {b}")
                print(f"位于： {a[0]} 目录下")
                # 记录日志信息
                logging.debug(f"已读入字典文件： {b}")
                logging.debug(f"位于： {a[0]} 目录下")
                # 如果需要在控制台查看字典内容，取消注释以下代码
                # print(trans_dict)
        except IOError as ioe:
            # 记录错误日志信息
            logging.error(f"未能读取字典文件： {file_path}\n错误原因{ioe}")
            # 打印错误信息
            print(f"未能读取字典文件： {file_path}\n错误原因{ioe}")


# 获取RVTools报告文件
def RVtools_file(ip):  # 定义一个名为RVtools_file的函数，接收一个参数ip
    global rvtools_flag  # 声明全局变量rvtools_flag
    global rvtools_data_vTools  # 声明全局变量rvtools_data_vTools
    global rvtools_data_vSnapshot  # 声明全局变量rvtools_data_vSnapshot
    global rvtools_data_vCluster  # 声明全局变量rvtools_data_vCluster

    print("启动深度搜索模式")  # 打印启动深度搜索模式的信息
    if RVtools_deep_search(ip):  # 如果RVtools_deep_search函数返回True，执行以下代码
        return 1  # 返回1

    for _current_folder, _list_folders, _files in os.walk(current_dir):  # 遍历当前目录下的所有文件夹和文件
        for rvtools_file in _files:  # 遍历当前目录下的所有文件
            if fnmatch.fnmatch(rvtools_file, f'RVTools_export*{ip}*'):  # 如果文件名匹配RVTools_export*{ip}*的模式
                rvtools_flag = True  # 将rvtools_flag设置为True
                print(f"发现RVtools报告： {rvtools_file}")  # 打印发现RVtools报告的信息
                print(f"位于： {_current_folder} 目录下")  # 打印文件所在的目录信息
                logging.debug(f"发现RVtools报告： {rvtools_file}")  # 记录日志，发现RVtools报告的信息
                logging.debug(f"位于： {_current_folder} 目录下")  # 记录日志，文件所在的目录信息
                rvtools_data_vTools = pd.read_excel(rf"{os.path.join(_current_folder, rvtools_file)}",
                                                    sheet_name="vTools", header=0).fillna(' ')  # 读取Excel文件中的vTools表格数据，填充缺失值
                rvtools_data_vSnapshot = pd.read_excel(rf"{os.path.join(_current_folder, rvtools_file)}",
                                                       sheet_name="vSnapshot", header=0).fillna(' ')  # 读取Excel文件中的vSnapshot表格数据，填充缺失值
                rvtools_data_vCluster = pd.read_excel(rf"{os.path.join(_current_folder, rvtools_file)}",
                                                      sheet_name="vCluster", header=0).fillna(' ')  # 读取Excel文件中的vCluster表格数据，填充缺失值
                return 1  # 返回1
    if not rvtools_flag:  # 如果rvtools_flag为False
        print("未发现RVtools报告")  # 打印未发现RVtools报告的信息
        return 0  # 返回0


# RVTools报告文件深度搜索模式
def RVtools_deep_search(ip):
    global rvtools_flag  # 全局变量，用于标记是否找到RVtools报告
    global rvtools_data_vTools  # 全局变量，用于存储vTools数据
    global rvtools_data_vSnapshot  # 全局变量，用于存储vSnapshot数据
    global rvtools_data_vCluster  # 全局变量，用于存储vCluster数据

    # os.walk()函数用于遍历目录树，返回一个生成器，每次迭代返回一个三元组(dirpath, dirnames, filenames)
    # current_dir为要遍历的目录路径
    for _current_folder, _list_folders, _files in os.walk(current_dir):
        # 遍历当前目录下的所有文件
        for rvtools_file in _files:
            # 使用fnmatch模块匹配文件名，只处理xlsx格式的文件
            if fnmatch.fnmatch(rvtools_file, f'*xlsx'):
                # 读取Excel文件，获取"vMetaData"工作表的数据，并填充缺失值
                deep_search_source = pd.read_excel(rf"{os.path.join(_current_folder, rvtools_file)}",
                                                    sheet_name="vMetaData", header=0).fillna(' ')
                # 遍历"vMetaData"工作表的每一列
                for c in range(len(deep_search_source.columns)):
                    # 如果列名为"Server"
                    if deep_search_source.columns[c] == 'Server':
                        # 遍历"vMetaData"工作表的每一行
                        for i in deep_search_source.index:
                            # 如果该行的"Server"列的值等于输入的ip地址
                            if deep_search_source.values[i, c] == ip:
                                # 设置rvtools_flag为True，表示找到了RVtools报告
                                rvtools_flag = True
                                # 打印成功信息和日志
                                print(f"深度搜索成功，发现RVtools报告： {rvtools_file}")
                                print(f"位于： {_current_folder} 目录下")
                                logging.debug(f"发现RVtools报告： {rvtools_file}")
                                logging.debug(f"位于： {_current_folder} 目录下")
                                # 读取vTools、vSnapshot和vCluster工作表的数据，并填充缺失值
                                rvtools_data_vTools = pd.read_excel(rf"{os.path.join(_current_folder, rvtools_file)}",
                                                                    sheet_name="vTools", header=0).fillna(' ')
                                rvtools_data_vSnapshot = pd.read_excel(rf"{os.path.join(_current_folder, rvtools_file)}",
                                                                       sheet_name="vSnapshot", header=0).fillna(' ')
                                rvtools_data_vCluster = pd.read_excel(rf"{os.path.join(_current_folder, rvtools_file)}",
                                                                      sheet_name="vCluster", header=0).fillna(' ')
                                # 返回1，表示已经处理完当前文件，可以继续处理下一个文件
                                return 1
    # 如果遍历完所有文件都没有找到RVtools报告，打印未找到信息，并返回0
    if not rvtools_flag:
        print("深度搜索未发现RVtools报告")
        return 0


def template_check(template_name):
    # 声明全局变量template_flag，用于标记是否使用外置模板
    global template_flag

    # 如果传入的模板名称不为空
    if template_name != '':
        # 如果模板名称的第一个字符是双引号，则去掉第一个字符
        if template_name[0] == '"':
            template_name = template_name[1:]
        # 如果模板名称的最后一个字符是双引号，则去掉最后一个字符
        if template_name[-1] == '"':
            template_name = template_name[:-1]

    # 检查模板文件是否存在于当前目录或指定目录下
    if os.path.exists(rf"{template_name}") or os.path.exists(
            rf"{os.path.join(current_dir, template_name)}"):
        # 如果存在，则打印提示信息并记录日志，同时将template_flag设置为True
        print(f"使用外置模板： 开启\n模板文件已找到： {template_name}")
        logging.debug(f"使用外置模板： 开启\n模板文件已找到： {template_name}")
        template_flag = True
    else:
        # 如果不存在，则打印提示信息并记录日志，同时将template_flag设置为False
        print("使用外置模板： 关闭")
        logging.debug(f"使用外置模板： 关闭\n找不到外置模板文件")


# 定义翻译功能，优先检查字典文件中该语句是否已翻译过，如果没有再使用百度翻译
def translate(original_text):
    global dict_flag, translate_count  # 声明全局变量
    dict_flag = False  # 初始化字典标志为False
    model = re.compile("[0-9]+")  # 定义正则表达式模式，用于匹配数字
    source_text = original_text  # 将原始文本赋值给source_text
    list_source_text = model.findall(source_text)  # 从source_text中提取数字列表
    for i in range(len(list_source_text)):  # 遍历数字列表
        # 将数字出现的顺序替换为'$+(字母)'，如第一个出现的数字为'$a'，第二个为'$b'，以此类推
        source_text = source_text.replace(list_source_text[i], f'${chr(97 + i)}', 1)
    for key, value in trans_dict.items():  # 遍历翻译字典
        if str(source_text) == key:  # 如果source_text在字典的键中
            dict_flag = True  # 设置字典标志为True
            result = value  # 获取对应的值
            for i in range(len(list_source_text)):  # 遍历数字列表
                result = result.replace(f'${chr(97 + i)}', list_source_text[i])  # 将字母替换为对应的数字
            logging.debug(f"字典中读入： {key} → {result}")  # 记录日志
            return result  # 返回结果

    if not dict_flag:  # 如果字典标志仍为False
        translate_result = baidu_fanyi(original_text)  # 调用百度翻译函数进行翻译
        list_source = model.findall(original_text)  # 从原始文本中提取数字列表
        list_result = model.findall(translate_result)  # 从翻译结果中提取数字列表
        for i in range(len(list_source)):  # 遍历数字列表
            original_text = original_text.replace(list_source[i], f'${chr(97 + i)}', 1)  # 将数字替换为对应的字母
        for i in range(len(list_result)):  # 遍历数字列表
            translate_result = translate_result.replace(list_result[i], f'${chr(97 + i)}', 1)  # 将字母替换为对应的数字
        logging.debug(f"翻译完成，向字典中写入 \"{original_text}:{translate_result}\"")  # 记录日志
        translate_count += 1  # 翻译计数加1
        trans_dict[original_text] = translate_result  # 将原始文本和翻译结果添加到字典中
        try:
            for key, value in trans_dict.items():  # 遍历翻译字典
                if fnmatch.fnmatch(source_text, key):  # 如果source_text与字典的键匹配
                    dict_flag = True  # 设置字典标志为True
                    result = value  # 获取对应的值
                    for i in range(len(list_source_text)):  # 遍历数字列表
                        result = result.replace(f'${chr(97 + i)}', list_source_text[i])  # 将字母替换为对应的数字
                    logging.debug(f"字典中读入： {key} → {result}")  # 记录日志
                    return result  # 返回结果
        except Exception as e:
            logging.error(e)  # 记录错误日志
            return None  # 返回None


# 百度翻译功能，自动重试5次，每次等待1秒
@retry(tries=5, delay=1)
def baidu_fanyi(source_text):
    translate_text = source_text.split('\n')
    result_text = ""
    for translate_str in translate_text:
        http_client = None
        url = '/api/trans/vip/translate'
        from_lang = 'auto'
        to_lang = 'zh'
        salt = random.randint(32768, 65536)
        sign = hashlib.md5((appid + translate_str + str(salt) + secret_key).encode()).hexdigest()
        url = f"{url}?appid={appid}&q={urllib.parse.quote(translate_str)}&from={from_lang}&to={to_lang}&salt={str(salt)}&sign={sign}"
        try:
            http_client = http.client.HTTPConnection('api.fanyi.baidu.com')
            http_client.request('GET', url)
            response = http_client.getresponse()
            result_all = response.read().decode("utf-8")
            result = json.loads(result_all)
            result_text = result_text + result['trans_result'][0]['dst'] + '\n'
        except Exception as e:
            logging.error(f"翻译失败，错误{e}")
            print(f"翻译失败，错误{e}")
            return None
        finally:
            if http_client:
                http_client.close()
    result_text = str(result_text).lstrip('\n').rstrip('\n')
    # trans_dict[source_text] = result_text
    return result_text


# 定义一个排序函数，用于模仿Excel内置的排序算法
def sort_key(c):
    # 将输入字符串转换为小写形式
    c = c.lower()
    # 使用正则表达式匹配数字，并编译成模式对象
    re_digits = re.compile(r'(\d+)')
    # 使用正则表达式分割字符串，得到一个列表
    pieces = re_digits.split(c)
    # 将列表中奇数位置的元素转换为整数类型
    pieces[1::2] = map(int, pieces[1::2])
    # 返回处理后的列表作为排序依据
    return pieces


# 定义一个函数，用于在段落下方添加长横线
def add_horizontal_line_after_paragraph(par):
    # 获取段落元素
    p = par._element
    # 获取或添加段落属性
    pPr = p.get_or_add_pPr()
    # 创建一个底部边框元素
    pBdr = OxmlElement("w:pBdr")
    # 创建一个底部线条元素
    bottom_line = OxmlElement("w:bottom")
    # 设置底部线条的属性
    bottom_line.set(qn("w:val"), "single")  # 线条类型为单条
    bottom_line.set(qn("w:sz"), "6")  # 线条大小为6磅
    bottom_line.set(qn("w:space"), "1")  # 线条与文本之间的间距为1磅
    bottom_line.set(qn("w:color"), "auto")  # 线条颜色自动调整
    # 将底部线条添加到底部边框中
    pBdr.append(bottom_line)
    # 将底部边框添加到段落属性中
    pPr.append(pBdr)


# 段落设置功能，可以快速为段落设置: 字体、字号、段前间距、段后间距、多倍行距倍数、加粗、特殊修改、首行缩进、字体颜色
def paragraph_set(paragraph, font_name=None, font_size=None,
                  space_before=None, space_after=None, line_spacing_multiple=None,
                  bold=None, sp=True, indent=None, color=None):
    # 如果段落中没有run，添加一个空格run
    if len(paragraph.runs) == 0:
        paragraph.add_run(' ')

    # 遍历段落中的每个run
    for run in paragraph.runs:
        # 如果没有指定字体名称，使用默认的段落字体
        if not font_name:
            font_name = default_paragraph_font
        # 设置run的字体名称
        run.font.name = font_name
        # 设置run的字体名称属性
        run._element.rPr.rFonts.set(qn('w:eastAsia'), font_name)
        # 如果没有指定字体大小，使用默认的段落字体大小
        if not (font_size or font_size == 0):
            font_size = default_paragraph_font_size
        # 设置run的字体大小
        run.font.size = Pt(font_size)
        # 设置run的字体颜色
        run.font.color.rgb = color or RGBColor(0, 0, 0)
        # 如果指定了加粗，设置run的加粗属性
        if bold:
            run.bold = bold
    # 如果指定了缩进，设置段落的首行缩进
    if indent:
        paragraph.paragraph_format.first_line_indent = Pt(paragraph.runs[0].font.size.pt * indent)
    # 如果指定了更新样式，调用update_style函数更新段落样式
    if sp:
        update_style(paragraph.style)
    # 如果指定了前置空格，设置段落的前置空格
    if space_before:
        paragraph.paragraph_format.space_before = Pt(space_before)
    else:
        paragraph.paragraph_format.space_before = Pt(0)
    # 如果指定了后置空格，设置段落的后置空格
    if space_after:
        paragraph.paragraph_format.space_after = Pt(space_after)
    else:
        paragraph.paragraph_format.space_after = Pt(0)
    # 设置段落的行间距规则为多倍行距
    paragraph.paragraph_format.line_spacing_rule = WD_LINE_SPACING.MULTIPLE
    # 如果指定了行间距倍数，设置段落的行间距
    if line_spacing_multiple:
        paragraph.paragraph_format.line_spacing = line_spacing_multiple
    elif line_spacing_multiple == 0:
        paragraph.paragraph_format.line_spacing = 0
    else:
        paragraph.paragraph_format.line_spacing = 1


# 特殊修改，可以解决由原Word给部分标题设定的格式而导致无法设置中文字体的问题
def update_style(style):
    # 定义一个集合，包含需要从style.element.rPr.rFonts.attrib字典中删除的键
    attrib_delete = {'majorHAnsi', 'majorEastAsia', 'majorBidi'}
    # 遍历style.element.rPr.rFonts.attrib字典中的每一个键值对
    for key, value in style.element.rPr.rFonts.attrib.items():
        # 如果当前键对应的值在attrib_delete集合中，那么从字典中删除这个键值对
        if value in attrib_delete:
            style.element.rPr.rFonts.attrib.pop(key, None)


# 单元格设置功能，可以快速设置: 单元格文字内容、水平居中、垂直居中、宽度、字体颜色、单元格底色、加粗、字体、字号、段前间距、段后间距、多倍行距倍数
# 定义一个名为cell_set的函数，用于设置单元格的属性
def cell_set(cell, text=None, alignment_flag=True, vertical_flag=True, width=None,
             font_color=None, cell_color=None, b=False, font_name=None,
             font_size=None, space_before=None, space_after=None, line_spacing_multiple=None):
    # 如果字体名称未指定，则使用默认的表格段落字体
    if not font_name:
        font_name = default_table_paragraph_font
    # 如果字体大小未指定，则使用默认的段落字体大小
    if not font_size:
        font_size = default_paragraph_font_size
    # 如果宽度已指定，则设置单元格的宽度
    if width:
        cell.width = width
    # 如果文本已指定，则设置单元格的文本内容
    if text:
        cell.text = str(text)
    # 遍历单元格中的所有段落
    for paragraph in cell.paragraphs:
        # 如果对齐标志为真，则将段落的对齐方式设置为居中
        if alignment_flag:
            paragraph.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
        # 调用paragraph_set函数设置段落的其他属性
        paragraph_set(paragraph, font_name, font_size,
                      space_before, space_after, line_spacing_multiple,
                      b, False, None, font_color or RGBColor(0, 0, 0))
    # 如果垂直对齐标志为真，则将单元格的垂直对齐方式设置为居中
    if vertical_flag:
        cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
    # 如果单元格颜色已指定，则设置单元格的颜色
    if cell_color is not None:
        set_cell_color(cell, cell_color)


# 设置单元格底色
def set_cell_color(cell, cell_color):
    cell._tc.get_or_add_tcPr().append(
        parse_xml(r'<w:shd {} w:fill="{bgColor}"/>'.format(nsdecls('w'), bgColor=cell_color)))


# 根据表格首行各单元格的宽度，同步每列所有单元格的宽度，防止表格宽度异常，但无法处理合并的单元格
def sync_width(table):
    for row_idx in range(len(table.rows)):
        for cell_idx in range(len(table.rows[row_idx].cells)):
            table.rows[row_idx].cells[cell_idx].width = table.rows[0].cells[cell_idx].width


# 创建一个高度为0的空行，用于分离表格，防止两个表格粘连影响函数逻辑
def segmentation(output_file):
    paragraph = output_file.add_paragraph(' ')
    paragraph_set(paragraph, None, 0, 0, 0, 0, False, False, 0)


# 最终处理，关闭所有表格的跨页断行功能保证美观，更新目录（如果存在模板），设定页眉顶端距离及页脚底端距离
def final_proc(p):
    # 创建一个Word应用程序对象
    word = ws.gencache.EnsureDispatch('Word.Application')
    # 设置Word应用程序为不可见
    word.Visible = False
    # 禁止显示警告信息
    word.DisplayAlerts = False
    # 打开指定路径的文档
    doc = word.Documents.Open(p)
    # 定义厘米到磅的转换系数
    cm_to_points = 28.35
    try:
        # 遍历文档中的所有表格，禁止跨页断行
        for table in doc.Tables:
            table.Rows.AllowBreakAcrossPages = False
        # 设置页脚和页眉距离
        doc.PageSetup.FooterDistance = 1.75 * cm_to_points
        doc.PageSetup.HeaderDistance = 1.5 * cm_to_points
        # 如果模板标志为真，则更新目录并设置字体名称
        if template_flag:
            toc = doc.TablesOfContents(1)
            toc.Update()
            toc_range = toc.Range
            toc_range.Font.Name = default_heading_font
            # 遍历目录中的每个段落，设置字体名称
            for i in range(1, toc_range.Paragraphs.Count):
                toc_item_range = toc_range.Paragraphs(i).Range
                toc_item_range.Font.Name = "等线"
        # 如果模板标志为假，则添加页码
        else:
            add_page_numbers(doc)
    # 捕获异常并记录错误日志
    except Exception as err:
        logging.error(err)
    # 最后，保存文档、关闭文档并退出Word应用程序
    finally:
        doc.Save()
        doc.Close()
        word.Quit()


# 如果没有模板文件，则依赖该函数生成页码
def add_page_numbers(doc):
    # 获取页脚
    footer = doc.Sections(1).Footers(ws.constants.wdHeaderFooterPrimary).Range
    footer.Font.Size = 9
    footer.Bold = True
    footer.ParagraphFormat.LineSpacing = 12.96
    footer.ParagraphFormat.SpaceAfter = 8
    footer.ParagraphFormat.Alignment = ws.constants.wdAlignParagraphCenter
    footer.Collapse(0)
    footer.Fields.Add(footer, ws.constants.wdFieldPage)
    footer = doc.Sections(1).Footers(ws.constants.wdHeaderFooterPrimary).Range
    footer.Collapse(0)
    footer.InsertAfter(Text=' / ')
    footer.Collapse(0)
    footer.Fields.Add(footer, ws.constants.wdFieldNumPages)


# 本程序关键，生成巡检报告
def analysis_data(input_file, output_file):
    # 设置全局默认字体
    # output_file.styles["Normal"].font.name = default_paragraph_font
    # output_file.styles["Normal"]._element.rPr.rFonts.set(qn('w:eastAsia'), default_paragraph_font)
    # output_file.styles["Normal"].font.color.rgb = black_font
    # output_file.styles["Normal"].font.size = Pt(default_paragraph_font_size)

    # 定义部分变量
    p1 = p2 = p3 = ok = 0
    records = ()
    vcenter_ip = ""
    vcenter_version = ""
    is_vSAN = False
    clusters = []

    # 获取VHA原文档中全部表格
    tables = input_file.tables
    pars = input_file.paragraphs

    # 从关键表格中获取当前vCenter的版本号及IP地址
    for table in tables:
        if fnmatch.fnmatch(table.rows[0].cells[0].text, "VMware vCenter * - *"):
            title_text = table.rows[0].cells[0].text
            ip_pattern = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
            vcenter_ip = re.search(ip_pattern, title_text).group()
            version_pattern = r'\d+\.\d+\.\d+'
            vcenter_version = re.search(version_pattern, title_text).group()
            break
    RVtools_file(vcenter_ip)
    print(f"开始处理 {vcenter_ip} 的巡检报告，该 vSphere 的版本是 {vcenter_version}")
    logging.info(f"开始处理 {vcenter_ip} 的巡检报告，该 vSphere 的版本是 {vcenter_version}")
    vCenter_num = DataCenter_num = Cluster_num = ESXiServer_num = VirtualMachine_num = Datastore_num = 0
    # 获取问题表格中的所有数据并记录到元组中
    for table in tables:
        if table.rows[0].cells[0].text == "vCenter":
            print(f"已找到问题表格，该表格是原文档中第{tables.index(table) + 1}个表格，正在处理...")
            logging.info(f"已找到问题表格，该表格是原文档中第{tables.index(table) + 1}个表格，正在处理...")
            time.sleep(0.2)
            for i in tqdm(range(1, len(table.rows)), desc="获取关键数据", ncols=100):
                row = table.rows[i]
                if row.cells[1].text == "OK":
                    ok += 1
                    continue
                else:
                    num = i
                    priority = row.cells[1].text
                    component = row.cells[2].text
                    if component == 'vSAN' and not is_vSAN:
                        is_vSAN = True
                    for old, new in replace_dict.items():
                        component = component.replace(old, new)
                    if trans_flag:
                        observe = translate(
                            find_practice_observation(extract_hyperlink_text(row.cells[3]), tables))
                        recommended = translate(row.cells[4].text)
                    else:
                        observe = find_practice_observation(extract_hyperlink_text(row.cells[3]), tables)
                        recommended = row.cells[4].text
                    if priority == 'P1':
                        p1 += 1
                    elif priority == 'P2':
                        p2 += 1
                    elif priority == 'P3':
                        p3 += 1
                    record = (num, priority, component, observe, recommended)
                    records += (record,)
        if fnmatch.fnmatch(table.rows[0].cells[0].text, "VMware vCenter*{}".format(vcenter_ip)):
            DataCenter_num += int(table.rows[1].cells[2].text)
            Cluster_num += int(table.rows[2].cells[2].text)
            ESXiServer_num += int(table.rows[3].cells[2].text)
            VirtualMachine_num += int(table.rows[4].cells[2].text)
            Datastore_num += int(table.rows[5].cells[2].text)
            vCenter_num += 1
        if table.rows[0].cells[0].text == "Cluster":
            for i in range(1, len(table.rows)):
                if table.cell(i, 0).text == "StandAlone":
                    continue
                else:
                    clusters.append(table.cell(i, 0).text)

    clusters.sort(key=sort_key)

    if is_vSAN:
        print(f"该 vSphere {vcenter_version} 包含 vSAN")

    print(
        f"数据中心: {DataCenter_num} 群集: {Cluster_num} ESXi服务器: {ESXiServer_num} 虚拟机: {VirtualMachine_num} 数据存储: {Datastore_num}")
    print(f"该虚拟化平台共有{Cluster_num}个群集: {'、'.join(clusters)}。")
    logging.info(
        f"数据中心: {DataCenter_num} 群集: {Cluster_num} ESXi服务器: {ESXiServer_num} 虚拟机: {VirtualMachine_num} 数据存储: {Datastore_num}")
    logging.info(f"该虚拟化平台共有{Cluster_num}个群集: {'、'.join(clusters)}。")

    for paragraph in output_file.paragraphs:
        if paragraph.text == "环境健康分析巡检报告":
            if is_vSAN:
                paragraph.text = f"{customer} vSphere {vcenter_version} 与 vSAN\n环境健康分析巡检报告"
            else:
                paragraph.text = f"{customer} vSphere {vcenter_version} \n环境健康分析巡检报告"
            paragraph_set(paragraph, '宋体', 24, 0, 10, 1.15, color=RGBColor(68, 114, 196))
            break

    for paragraph in output_file.paragraphs:
        if paragraph.text == "年月日":
            paragraph.text = f"{d1.year}年{d1.month}月{d1.day}日"
            paragraph_set(paragraph, '宋体', 20, 0, 10, 1.15)
            break

    output_file.add_section()
    heading = output_file.add_heading("1 概要", level=1)
    paragraph_set(heading, default_heading_font, default_heading_1_font_size, 18, 8, 1.08, True, True)
    add_horizontal_line_after_paragraph(heading)

    output_file.add_paragraph()

    if is_vSAN:
        paragraph = output_file.add_paragraph(
            f"本报告详细描述了针对{customer} VMware vSphere {vcenter_version} with vSAN 虚拟化环境 vCenter {vcenter_ip} 健康检查服务内容，此服务包括评估当前 VMware vSphere with vSAN 平台的部署配置、操作和使用方面的评估。此报告的目的是发现、分析和基于健康检查的结果提供改善建议。")
    else:
        paragraph = output_file.add_paragraph(
            f"本报告详细描述了针对{customer} VMware vSphere {vcenter_version} 虚拟化环境 vCenter {vcenter_ip} 健康检查服务内容，此服务包括评估当前 VMware vSphere 平台的部署配置、操作和使用方面的评估。此报告的目的是发现、分析和基于健康检查的结果提供改善建议。")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, indent=2)
    paragraph = output_file.add_paragraph(
        "通过 VMware 健康检查了解 vSphere 各环节运行状况，这份报告总结了 vSphere 健康检查的发现和建议。检查报告主要包括: ")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, indent=2)
    paragraph = output_file.add_paragraph(
        "1.记录检查系统的分类问题清单；")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, indent=2)
    paragraph = output_file.add_paragraph(
        "2.检查评估活动执行跟踪；")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, indent=2)
    paragraph = output_file.add_paragraph(
        "3.根据 VMware 最佳做法，提供关于配置和使用方面，用于改进利用率和操作的详细建议；")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, indent=2)
    paragraph = output_file.add_paragraph(
        "4.帮助衡量虚拟基础架构的配置和使用情况与 VMware 最佳做法之间的差距，以及确定需要注意或修正的地方。")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, indent=2)

    if page_break_flag:
        output_file.add_page_break()

    heading = output_file.add_heading("2 巡检报告概述", level=1)
    paragraph_set(heading, default_heading_font, default_heading_1_font_size, 18, 8, 1.08, True, True)
    add_horizontal_line_after_paragraph(heading)
    paragraph = output_file.add_paragraph(
        "本健康报告记录了虚拟化平台架构的总体健康情况，健康检查报告包括虚拟化环境是否符合 VMware 最佳做法的报告，指出了配置、操作、使用和规划方面的问题。还包括了需要注意或进一步调查的项目、详细建议和技术支持信息。")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, indent=2)
    paragraph = output_file.add_paragraph(
        "本文健康检查报告卡将使用以下级别表述评估结果:")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, indent=2)

    level_table = output_file.add_table(6, 2, style='Table Grid')
    level_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
    level_table.autofit = False  # 表格自动调整
    cells_1 = level_table.rows[0].cells
    cell_set(cells_1[0], '等级', True, True, Cm(3.4), cell_color=title_color, b=True,
             font_name=default_table_paragraph_font)
    cell_set(cells_1[1], '定义', True, True, Cm(11.6), cell_color=title_color, b=True,
             font_name=default_table_paragraph_font)
    cells_2 = level_table.rows[1].cells
    cell_set(cells_2[0], 'P1', True, True, Cm(3.4), cell_color=p1_color, font_name=default_table_paragraph_font)
    cell_set(cells_2[1], "需要立即注意的重点项目，并采取相应行动解决每个重点项目。", False, True, Cm(11.6),
             font_name=default_table_paragraph_font)
    cells_3 = level_table.rows[2].cells
    cell_set(cells_3[0], 'P2', True, True, Cm(3.4), cell_color=p2_color, font_name=default_table_paragraph_font)
    cell_set(cells_3[1], "可能需要关注的项目。这些项目要么不关键，要么需要进一步调查。", False, True, Cm(11.6),
             font_name=default_table_paragraph_font)
    cells_4 = level_table.rows[3].cells
    cell_set(cells_4[0], 'P3', True, True, Cm(3.4), cell_color=p3_color, font_name=default_table_paragraph_font)
    cell_set(cells_4[1], "偏离最佳实践的项目，但解决这些问题可能不是最优先的事项。", False, True, Cm(11.6),
             font_name=default_table_paragraph_font)
    cells_5 = level_table.rows[4].cells
    cell_set(cells_5[0], 'OK', True, True, Cm(3.4), cell_color=ok_color, font_name=default_table_paragraph_font)
    cell_set(cells_5[1], "符合最佳实践指南的项目，无需担心和关注这些项目。", False, True, Cm(11.6),
             font_name=default_table_paragraph_font)
    cells_6 = level_table.rows[5].cells
    cell_set(cells_6[0], '不适用', True, True, Cm(3.4), cell_color=no_color, font_name=default_table_paragraph_font)
    cell_set(cells_6[1], "本项目不适用", False, True, Cm(11.6), font_name=default_table_paragraph_font)
    for i in range(0, len(level_table.rows)):
        level_table.rows[i].height = Cm(1.5)

    heading = output_file.add_heading('2.1 检查清单', level=2)
    paragraph_set(heading, default_heading_font, default_heading_2_font_size, 18, 0, 1.08, True, True)
    paragraph = output_file.add_paragraph(
        f"本次检查涉及 vCenter Server {vcenter_ip} 管理的基础架构的设备和对象，检查清单内容包含以下数量:")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False, 2)
    heading = output_file.add_heading('表1: 检查清单', level=9)
    paragraph_set(heading, default_paragraph_font, 9, 0, 10, 1, False, True)

    check_table = output_file.add_table(2, 6, style='Medium Grid 3 Accent 5')
    check_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
    check_table.autofit = False  # 表格自动调整
    head_cells = check_table.rows[0].cells
    cell_set(head_cells[0], 'vCenter', True, True, Cm(2.5), white_font, b=True, font_name=default_table_paragraph_font)
    cell_set(head_cells[1], '数据中心', True, True, Cm(2.5), white_font, b=True, font_name=default_table_paragraph_font)
    cell_set(head_cells[2], '群集', True, True, Cm(2.5), white_font, b=True, font_name=default_table_paragraph_font)
    cell_set(head_cells[3], 'ESXi主机', True, True, Cm(2.5), white_font, b=True, font_name=default_table_paragraph_font)
    cell_set(head_cells[4], '数据存储', True, True, Cm(2.5), white_font, b=True, font_name=default_table_paragraph_font)
    cell_set(head_cells[5], '虚拟机', True, True, Cm(2.5), white_font, b=True, font_name=default_table_paragraph_font)
    check_cells = check_table.rows[1].cells
    cell_set(check_cells[0], str(vCenter_num), True, True, font_color=white_font, b=True,
             font_name=default_table_paragraph_font)
    cell_set(check_cells[1], str(DataCenter_num), font_name=default_table_paragraph_font)
    cell_set(check_cells[2], str(Cluster_num), font_name=default_table_paragraph_font)
    cell_set(check_cells[3], str(ESXiServer_num), font_name=default_table_paragraph_font)
    cell_set(check_cells[4], str(Datastore_num), font_name=default_table_paragraph_font)
    cell_set(check_cells[5], str(VirtualMachine_num), font_name=default_table_paragraph_font)
    check_table.rows[0].height = head_cells[0].paragraphs[0].runs[0].font.size * 2
    check_table.rows[1].height = check_cells[0].paragraphs[0].runs[0].font.size * 2
    sync_width(check_table)

    heading = output_file.add_heading('2.2 检查问题汇总', level=2)
    paragraph_set(heading, default_heading_font, default_heading_2_font_size, 18, 0, 1.08, True, True)
    paragraph = output_file.add_paragraph(
        "本次汇总突出显示了数据中心 vSphere 虚拟化基础设施的评估，评估摘要是由 VMware 健康分析器检查得出的。下图显示了所有项目的汇总: ")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False, 2)
    heading = output_file.add_heading('图1: 问题占比率', level=9)
    paragraph_set(heading, default_paragraph_font, 9, 0, 10, 1, False, True)

    # 由于未能找到合适地复制图表的方法，只能选择由程序来生成饼图或留下标记方便粘贴
    if chart_flag:
        labels = ['OK', 'P3', 'P2', 'P1']
        sizes = [ok, p3, p2, p1]
        colors = [f"#{ok_color}", f"#{p3_color}", f"#{p2_color}", f"#{p1_color}"]
        plt.pie(sizes, labels=labels, colors=colors, autopct='%1.1f%%', startangle=90)
        plt.axis('equal')
        plt.savefig(f'pie_chart_{vcenter_ip}.png')
        plt.close()
        output_file.add_picture(f'pie_chart_{vcenter_ip}.png', width=Cm(15.0))
    else:
        output_file.add_paragraph("{{图表}}")

    paragraph = output_file.add_paragraph(
        f"本次 vSphere 环境健康检查总共提供{p1 + p2 + p3}个整改建议，其中P1级别建议{p1}个，P2级别建议{p2}个，P3级别建议{p3}个。此外还有OK级别项目{ok}个，这些项目已是最佳状态，无需关注也不会列出。")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False, 2)
    paragraph = output_file.add_paragraph(
        "检查结果问题汇总及建议如下表所示:")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False)
    heading = output_file.add_heading('表2: 问题汇总及建议', level=9)
    paragraph_set(heading, default_paragraph_font, 9, 0, 10, 1, False, True)

    title = ['序号', '优先', '类型', '问题', '建议']
    title_width = [Cm(1.25), Cm(1.25), Cm(2), Cm(5.25), Cm(5.25)]
    new_table = output_file.add_table(1, 5, style='Table Grid')
    new_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
    new_table.autofit = False  # 表格自动调整
    hdr_cells = new_table.rows[0].cells
    print("正在生成检查问题表格")
    for i in range(len(new_table.rows[0].cells)):
        cell_set(hdr_cells[i], title[i], True, True, title_width[i],
                 None, title_color, True, default_table_paragraph_font, default_paragraph_font_size, 0, 8, 1.08)
    new_table.rows[0].height = hdr_cells[0].paragraphs[0].runs[0].font.size * 2
    time.sleep(0.2)
    for new_num, new_priority, new_component, new_observe, new_recommended in tqdm(records, total=len(records),
                                                                                   desc="生成表格", ncols=100):
        row_cells = new_table.add_row().cells
        cell_set(row_cells[0], str(new_num), True, True, font_name=default_table_paragraph_font)
        if new_priority == 'P1':
            cell_set(row_cells[1], new_priority, True, True, cell_color=p1_color,
                     font_name=default_table_paragraph_font)
        elif new_priority == 'P2':
            cell_set(row_cells[1], new_priority, True, True, cell_color=p2_color,
                     font_name=default_table_paragraph_font)
        elif new_priority == 'P3':
            cell_set(row_cells[1], new_priority, True, True, cell_color=p3_color,
                     font_name=default_table_paragraph_font)
        cell_set(row_cells[2], new_component, True, True, font_name=default_table_paragraph_font)
        cell_set(row_cells[3], new_observe, False, True, font_name=default_table_paragraph_font)
        cell_set(row_cells[4], new_recommended, False, True, font_name=default_table_paragraph_font)
    sync_width(new_table)

    if page_break_flag:
        output_file.add_page_break()

    heading = output_file.add_heading("3 虚拟化平台当前运行情况", level=1)
    paragraph_set(heading, default_heading_font, default_heading_1_font_size, 18, 8, 1.08, True)
    add_horizontal_line_after_paragraph(heading)
    heading = output_file.add_heading('3.1 群集信息', level=2)
    paragraph_set(heading, default_heading_font, default_heading_2_font_size, 18, 0, 1.08, True)
    paragraph = output_file.add_paragraph(
        f"该虚拟化平台共有{Cluster_num}个群集: {'、'.join(clusters)}。在下边表格显示的是这些群集的 CPU 和 RAM 资源的总数与有效值: ")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False, 2)

    title = ['Name', 'Num Hosts', 'Total CPU', 'Effective CPU', 'Total Memory', 'Effective Memory']
    cluster_table = output_file.add_table(1, len(title), style='Table Grid')
    cluster_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
    cluster_table.autofit = False  # 表格自动调整
    head_cells = cluster_table.rows[0].cells
    for i in range(len(title)):
        cell_set(head_cells[i], title[i], True, True, Cm(2.5),
                 white_font, "000000",
                 True, 'Calibri', 9, 0, 8, 1.08)
    if rvtools_flag:
        Name_columns = 0
        NumHosts_columns = 0
        TotalCPU_columns = 0
        EffectiveCPU_columns = 0
        TotalMemory_columns = 0
        EffectiveMemory_columns = 0
        for c in range(len(rvtools_data_vCluster.columns)):
            if rvtools_data_vCluster.columns[c] == 'Name':
                Name_columns = c
            elif rvtools_data_vCluster.columns[c] == 'NumHosts':
                NumHosts_columns = c
            elif rvtools_data_vCluster.columns[c] == 'TotalCpu':
                TotalCPU_columns = c
            elif rvtools_data_vCluster.columns[c] == 'Effective Cpu':
                EffectiveCPU_columns = c
            elif rvtools_data_vCluster.columns[c] == 'TotalMemory':
                TotalMemory_columns = c
            elif rvtools_data_vCluster.columns[c] == 'Effective Memory':
                EffectiveMemory_columns = c
        for i in range(len(rvtools_data_vCluster.index)):
            vc_Name = rvtools_data_vCluster.values[i, Name_columns]
            vc_NumHosts = rvtools_data_vCluster.values[i, NumHosts_columns]
            vc_TotalCPU = rvtools_data_vCluster.values[i, TotalCPU_columns]
            vc_EffectiveCPU = rvtools_data_vCluster.values[i, EffectiveCPU_columns]
            vc_TotalMemory = rvtools_data_vCluster.values[i, TotalMemory_columns]
            vc_EffectiveMemory = rvtools_data_vCluster.values[i, EffectiveMemory_columns]
            row_cells = cluster_table.add_row().cells
            data = [vc_Name, vc_NumHosts, vc_TotalCPU, vc_EffectiveCPU, vc_TotalMemory, vc_EffectiveMemory]
            for j in range(len(data)):
                cell_set(row_cells[j], data[j], False, False, None,
                         black_font, None,
                         False, 'Calibri', 9, 0, 8, 1.08)
    else:
        for i in range(Cluster_num):
            row_cells = cluster_table.add_row().cells
            cell_set(row_cells[0], clusters[i], False, False, None,
                     black_font, None,
                     False, 'Calibri', 9, 0, 8, 1.08)
        for i in range(1, len(cluster_table.rows)):
            for j in range(1, len(cluster_table.rows[i].cells)):
                cell = cluster_table.rows[i].cells[j]
                cell_set(cell, None, False, False, None,
                         black_font, None,
                         False, 'Calibri', 9, 0, 8, 1.08)
    sync_width(cluster_table)

    heading = output_file.add_heading('3.2 主机信息', level=2)
    paragraph_set(heading, default_heading_font, default_heading_2_font_size, 18, 0, 1.08, True)
    paragraph = output_file.add_paragraph(
        "以下统计为主机信息，包括设备类型，中央处理器(CPU)，运行内存(RAM)，存储适配器(HBA)，网卡(NIC)等: ")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False, 2)

    paragraph_position_last = 0
    table_position = 0
    for p in pars:
        paragraph_position = pars.index(p)
        if fnmatch.fnmatch(p.text,
                           "vCenter Server *. vCenterServer- *") and paragraph_position > paragraph_position_last:
            paragraph_position_last = pars.index(p)
            par = p.text
            for old, new in part_dict.items():
                par = par.replace(old, new)
            paragraph = output_file.add_paragraph(par)
            paragraph_set(paragraph, default_table_letter_font, 13.5, 9, 9, 1, True)
            for inventory_table in tables:
                if inventory_table.rows[0].cells[0].text == "vCenter Server" and tables.index(
                        inventory_table) > table_position:
                    table_position = tables.index(inventory_table)
                    print(f"已找到 vCenter服务器 的表格，该表格是原文档中第 {table_position + 1} 个表格，正在处理...")
                    logging.info(
                        f"已找到 vCenter服务器 的表格，该表格是原文档中第 {table_position + 1} 个表格，正在处理...")
                    new_inventory_table = output_file.add_table(1, 1, style='Table Grid')
                    new_inventory_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
                    new_inventory_table.autofit = False  # 表格自动调整
                    cell_set(new_inventory_table.rows[0].cells[0], "vCenter服务器",
                             False, True, Cm(15), None, title_color, True, default_table_paragraph_font, 12, 6, 6, 1.08)
                    row_cells = new_inventory_table.add_row().cells
                    texts = inventory_table.rows[1].cells[0].text.split('\n')
                    time.sleep(0.2)
                    for i in tqdm(range(0, len(texts)), desc="生成表格", ncols=100):
                        t = texts[i]
                        for old, new in part_dict.items():
                            t = t.replace(old, new)
                        row_cells[0].paragraphs[i].text = t
                        row_cells[0].paragraphs[i].style = 'List Bullet'
                        if i < len(texts) - 1:
                            row_cells[0].add_paragraph()
                    cell_set(row_cells[0], None,
                             False, False, None, None, None, False, default_table_paragraph_font,
                             default_paragraph_font_size, 5, 5, 1.08)
                    sync_width(new_inventory_table)
                    break

    host_count = 0
    paragraph_position_last = 0
    table_position = 0
    for p in pars:
        paragraph_position = pars.index(p)
        if fnmatch.fnmatch(p.text, "Host Configuration *") and paragraph_position > paragraph_position_last:
            paragraph_position_last = pars.index(p)
            host_count += 1
            paragraph = output_file.add_paragraph(f"主机配置{host_count}")
            paragraph_set(paragraph, default_paragraph_font, 13.5, 9, 9, 1, True)
            for host_table in tables:
                if host_table.rows[0].cells[0].text == "Platform Specifications" and tables.index(
                        host_table) > table_position:
                    table_position = tables.index(host_table)
                    print(
                        f"已找到 主机配置{host_count} 的表格，该表格是原文档中第 {table_position + 1} 个表格，正在处理...")
                    logging.info(
                        f"已找到 主机配置{host_count} 的表格，该表格是原文档中第 {table_position + 1} 个表格，正在处理...")
                    new_host_table = output_file.add_table(1, 1, style='Table Grid')
                    new_host_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
                    new_host_table.autofit = False  # 表格自动调整
                    cell_set(new_host_table.rows[0].cells[0], "平台规格",
                             False, True, Cm(15), None, title_color,
                             True, default_table_paragraph_font, 12, 6, 6, 1.08)
                    row_cells = new_host_table.add_row().cells
                    texts = host_table.rows[1].cells[0].text.split('\n')
                    time.sleep(0.2)
                    for i in tqdm(range(0, len(texts)), desc="生成表格", ncols=100):
                        t = texts[i]
                        for old, new in part_dict.items():
                            t = t.replace(old, new)
                        row_cells[0].paragraphs[i].text = t
                        row_cells[0].paragraphs[i].style = 'List Bullet'
                        if i < len(texts) - 1:
                            row_cells[0].add_paragraph()
                    cell_set(row_cells[0], None, False, False, None,
                             None, None, False, default_table_paragraph_font, default_paragraph_font_size, 5, 5, 1.08)
                    sync_width(new_host_table)
                    segmentation(output_file)
                    hosts_table = tables[table_position + 1]
                    new_hosts_table = output_file.add_table(1, 1, style='Table Grid')
                    new_hosts_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
                    new_hosts_table.autofit = False  # 表格自动调整
                    cell_set(new_hosts_table.rows[0].cells[0], "ESX/ESXi 主机",
                             False, True, Cm(15), None, title_color, True,
                             default_paragraph_font, 12, 6, 6, 1.08)
                    row_cells = new_hosts_table.add_row().cells
                    texts = hosts_table.rows[1].cells[0].text.split('\n')
                    time.sleep(0.2)
                    for i in tqdm(range(0, len(texts)), desc="生成表格", ncols=100):
                        t = texts[i]
                        for old, new in part_dict.items():
                            t = t.replace(old, new)
                        row_cells[0].paragraphs[i].text = t
                        row_cells[0].paragraphs[i].style = 'List Bullet'
                        if i < len(texts) - 1:
                            row_cells[0].add_paragraph()
                    cell_set(row_cells[0], None, False, False, None,
                             None, None, False, default_table_letter_font, default_letter_font_size, 5, 5, 1.08)
                    sync_width(new_hosts_table)
                    break

    heading = output_file.add_heading('3.3 数据存储信息', level=2)
    paragraph_set(heading, default_heading_font, default_heading_2_font_size, 18, 0, 1.08, True)

    for storage_table in tables:
        if storage_table.rows[0].cells[0].text == "Datastore Name":
            print(f"已找到 存储数据 的表格，该表格是原文档中第 {tables.index(storage_table) + 1} 个表格，正在处理...")
            logging.info(
                f"已找到 存储数据 的表格，该表格是原文档中第 {tables.index(storage_table) + 1} 个表格，正在处理...")
            storage_num = len(storage_table.rows) - 1
            paragraph = output_file.add_paragraph(
                f"该虚拟化平台一共有{storage_num}个数据存储器，下表主要统计各个数据存储器的容量和可用容量等: ")
            paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False, 2)
            new_storage_table = output_file.add_table(1, 5, style='Table Grid')
            new_storage_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
            new_storage_table.autofit = False  # 表格自动调整
            title = ['数据存储名称', '类型', '大小(GB)', '可用空间(GB)', '备注']
            for i in range(5):
                cell_set(new_storage_table.rows[0].cells[i], title[i],
                         False, True, Cm(3), None, title_color, True, default_table_paragraph_font,
                         default_paragraph_font_size, 5, 5, 1.08)
            time.sleep(0.2)
            for i in tqdm(range(1, len(storage_table.rows)), desc="生成表格", ncols=100):
                row_cells = new_storage_table.add_row().cells
                for j in range(0, len(storage_table.rows[i].cells)):
                    cell_set(row_cells[j], storage_table.rows[i].cells[j].text,
                             False, True, None, None, None, False, default_table_letter_font, default_letter_font_size,
                             5, 5, 1.08)
            sync_width(new_storage_table)
            break

    heading = output_file.add_heading('3.4 组网配置', level=2)
    paragraph_set(heading, default_heading_font, default_heading_2_font_size, 18, 0, 1.08, True)

    network_pos = 0
    network_count = 0
    for network_table in tables:
        key_name = network_table.rows[0].cells[0].text
        if network_count != 0 and tables.index(network_table) - network_pos > 2:
            break
        if fnmatch.fnmatch(key_name, "Networking Configuration *") and tables.index(
                network_table) > network_pos:
            network_pos = tables.index(network_table)
            network_count += 1
            paragraph = output_file.add_paragraph(f"网络配置{network_count}")
            paragraph_set(paragraph, default_paragraph_font, 13.5, 9, 9, 1, True)
            print(f"已找到 网络配置{network_count} 的表格，该表格是原文档中第 {network_pos + 1} 个表格，正在处理...")
            logging.info(
                f"已找到 网络配置{network_count} 的表格，该表格是原文档中第 {network_pos + 1} 个表格，正在处理...")
            new_network_table = output_file.add_table(1, 1, style='Table Grid')
            new_network_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
            new_network_table.autofit = False  # 表格自动调整
            cell_set(new_network_table.rows[0].cells[0], "网络配置信息",
                     False, True, Cm(15), None, title_color, True, default_table_paragraph_font, 12, 6, 6, 1.08)
            row_cells = new_network_table.add_row().cells
            texts = network_table.rows[1].cells[0].text.split('\n')
            for i in range(0, len(texts)):
                t = texts[i]
                for old, new in net_dict.items():
                    t = t.replace(old, new)
                row_cells[0].paragraphs[i].text = t
                if i < len(texts) - 1:
                    row_cells[0].add_paragraph()
            cell_set(row_cells[0], None,
                     False, False, None, None, None, False, default_table_paragraph_font, default_paragraph_font_size,
                     5, 5, 1.08)
            sync_width(new_network_table)
            segmentation(output_file)
            title = ['交换机名', '交换机类型', '总端口数', '可用端口数', '端口组', '活动网卡/上行链路',
                     '待命网卡/上行链路']
            networking_table = tables[network_pos + 1]
            new_networking_table = output_file.add_table(1, len(title), style='Table Grid')
            new_networking_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
            new_networking_table.autofit = False  # 表格自动调整
            for i in range(len(title)):
                cell_set(new_networking_table.rows[0].cells[i], title[i],
                         False, False, Cm(15) / len(title), None, title_color, True, default_table_paragraph_font,
                         default_paragraph_font_size, 5, 5,
                         1.08)
            time.sleep(0.2)
            for i in tqdm(range(1, len(networking_table.rows)), desc="生成表格", ncols=100):
                row_cells = new_networking_table.add_row().cells
                for j in range(0, len(networking_table.rows[i].cells)):
                    cell_set(row_cells[j], networking_table.rows[i].cells[j].text,
                             False, False, None, None, None, False, default_table_letter_font, default_letter_font_size,
                             5, 5, 1.08)
            sync_width(new_networking_table)

    heading = output_file.add_heading('3.5 VMTools', level=2)
    paragraph_set(heading, default_heading_font, default_heading_2_font_size, 18, 0, 1.08, True)
    paragraph = output_file.add_paragraph("下表是检测出未安装 VMTools 的虚拟机，建议尽快安装。")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False, 2)

    title = ['虚拟机名', '群集', '主机', 'VMTools状态']
    title_width = [Cm(4), Cm(3.5), Cm(3.5), Cm(4)]
    vmtools_table = output_file.add_table(1, len(title), style='Table Grid')
    vmtools_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
    vmtools_table.autofit = False  # 表格自动调整
    head_cells = vmtools_table.rows[0].cells
    for i in range(len(title)):
        cell_set(head_cells[i], title[i], True, True, title_width[i], None, title_color, True,
                 default_table_paragraph_font,
                 default_paragraph_font_size, 6,
                 6, 1.08)
    if rvtools_flag:
        print("正在处理RVTools报告文件内的VMTools数据")
        VM_columns = 0
        Tools_columns = 0
        Cluster_columns = 0
        Host_columns = 0
        time.sleep(0.2)
        for c in tqdm(range(len(rvtools_data_vTools.columns)), desc="获取关键数据", ncols=100):
            if rvtools_data_vTools.columns[c] == 'VM':
                VM_columns = c
            elif rvtools_data_vTools.columns[c] == 'Tools':
                Tools_columns = c
            elif rvtools_data_vTools.columns[c] == 'Cluster':
                Cluster_columns = c
            elif rvtools_data_vTools.columns[c] == 'Host':
                Host_columns = c
        if len(rvtools_data_vTools.index) == 0:
            var = vmtools_table.add_row().cells
            for i in range(len(title)):
                cell_set(var[i], ' ', False, False, title_width[i], None, None, False, default_table_letter_font,
                         default_letter_font_size, 5, 5, 1.08)
        else:
            time.sleep(0.2)
            for i in tqdm(range(len(rvtools_data_vTools.index)), desc="生成表格", ncols=100):
                if rvtools_data_vTools.values[i, Tools_columns] == 'toolsNotInstalled':
                    vm_name = rvtools_data_vTools.values[i, VM_columns]
                    vm_cluster = rvtools_data_vTools.values[i, Cluster_columns]
                    vm_host = rvtools_data_vTools.values[i, Host_columns]
                    vm_tools = rvtools_data_vTools.values[i, Tools_columns]
                    row_cells = vmtools_table.add_row().cells
                    data = [vm_name, vm_cluster, vm_host, vm_tools]
                    for j in range(len(data)):
                        cell_set(row_cells[j], data[j], False, False, title_width[j], None, None, False,
                                 default_table_letter_font, default_letter_font_size, 5, 5, 1.08)
    else:
        next_cells = vmtools_table.add_row().cells
        for i in range(len(title)):
            cell_set(next_cells[i], None, False, False, title_width[i], None, None, False, default_table_letter_font,
                     default_letter_font_size,
                     5, 5, 1.08)
    sync_width(vmtools_table)

    heading = output_file.add_heading('3.6 虚拟机快照', level=2)
    paragraph_set(heading, default_heading_font, default_heading_2_font_size, 18, 0, 1.08, True)
    paragraph = output_file.add_paragraph("下表是检测到有快照的虚拟机，建议尽量减少快照的使用、清理过旧的快照。")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False, 2)

    title = ['虚拟机名', '快照详细状态']
    title_width = [Cm(5), Cm(10)]
    snap_table = output_file.add_table(1, len(title), style='Table Grid')
    snap_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
    snap_table.autofit = False  # 表格自动调整
    head_cells = snap_table.rows[0].cells
    for i in range(len(title)):
        cell_set(head_cells[i], title[i], True, True, title_width[i], None, title_color, True,
                 default_table_paragraph_font,
                 default_paragraph_font_size, 6, 6, 1.08)
    if rvtools_flag:
        print("正在处理RVTools报告文件内的虚拟机快照数据")
        VM_Name = 0
        VM_Date = 0
        VM_Snapshot = 0
        time.sleep(0.2)
        for c in tqdm(range(len(rvtools_data_vSnapshot.columns)), desc="获取关键数据", ncols=100):
            if rvtools_data_vSnapshot.columns[c] == 'VM':
                VM_Name = c
            elif rvtools_data_vSnapshot.columns[c] == 'Date / time':
                VM_Date = c
            elif rvtools_data_vSnapshot.columns[c] == 'Filename':
                VM_Snapshot = c
        if len(rvtools_data_vSnapshot.index) == 0:
            var = snap_table.add_row().cells
            for i in range(len(title)):
                cell_set(var[i], ' ', False, False, title_width[i], None, None, False, default_table_paragraph_font,
                         default_paragraph_font_size, 5, 5, 1.08)
        else:
            time.sleep(0.2)
            for i in tqdm(range(len(rvtools_data_vSnapshot.index)), desc="生成表格", ncols=100):
                vm_name = rvtools_data_vSnapshot.values[i, VM_Name]
                vm_date = str(rvtools_data_vSnapshot.values[i, VM_Date])
                vm_snapshot = rvtools_data_vSnapshot.values[i, VM_Snapshot]
                d2 = dt.datetime.strptime(vm_date, "%Y-%m-%d %H:%M:%S").date()
                Days = (d1 - d2).days
                snapshot_detail = f"该虚拟机有与其关联的旧快照{vm_snapshot}(距报告出具日期{Days}天)。"
                row_cells = snap_table.add_row().cells
                data = [vm_name, snapshot_detail]
                for j in range(len(data)):
                    cell_set(row_cells[j], data[j], False, False, title_width[j], None, None, False,
                             default_table_paragraph_font, default_paragraph_font_size, 5, 5, 1.08)
    else:
        next_cells = snap_table.add_row().cells
        for i in range(len(title)):
            cell_set(next_cells[i], ' ', False, False, title_width[i], None, None, False, default_table_paragraph_font,
                     default_paragraph_font_size,
                     5, 5, 1.08)
    sync_width(snap_table)

    heading = output_file.add_heading('3.7 单次服务记录', level=2)
    paragraph_set(heading, default_heading_font, default_heading_2_font_size, 18, 0, 1.08, True)
    paragraph = output_file.add_paragraph(
        "下表是上一次巡检至本次巡检期间，我方工程师对不在维保范围内的主机进行单次服务的记录 (如维护、配置变更等) 。")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False, 2)

    title = ['品牌', '型号', 'IP', '故障说明', '处理说明', '备注']
    server_table = output_file.add_table(2, len(title), style='Table Grid')
    server_table.alignment = WD_TABLE_ALIGNMENT.CENTER  # 表格在页面上居中
    server_table.autofit = False  # 表格自动调整
    head_cells = server_table.rows[0].cells
    next_cells = server_table.rows[1].cells
    for i in range(len(title)):
        cell_set(head_cells[i], title[i], True, True, Cm(2.5), None, title_color, True, default_table_paragraph_font,
                 default_paragraph_font_size, 6,
                 6, 1.08)
        cell_set(next_cells[i], None, False, False, Cm(2.5), None, None, True, default_table_paragraph_font,
                 default_paragraph_font_size, 6, 6, 1.08)
    sync_width(server_table)

    if page_break_flag:
        output_file.add_page_break()

    heading = output_file.add_heading("4 健康检查评估最佳实践", level=1)
    paragraph_set(heading, default_heading_font, default_heading_1_font_size, 18, 8, 1.08, True)
    add_horizontal_line_after_paragraph(heading)
    paragraph = output_file.add_paragraph(
        "以下是所有当前 vSphere 最佳实践的列表。")
    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False, False, 2)
    paragraph = output_file.add_paragraph(
        f"VMware vCenter {vcenter_version}")
    paragraph_set(paragraph, 'Arial', 13.5, 0, 8, 1.08, True, False, 2)

    num = 0
    for p in pars:
        if fnmatch.fnmatch(p.text, "Component: *"):
            num += 1
            p_pos = pars.index(p)
            par = p.text.split(': ')[1]
            for old, new in replace_dict.items():
                par = par.replace(old, new)
            heading = output_file.add_heading(f'4.{num} 组件: {par}', level=2)
            paragraph_set(heading, default_heading_font, default_heading_2_font_size, 18, 6, 1.08, True, indent=1)
            print(f"找到了 组件: {par} 位于原文档第{p_pos}段，正在处理...")
            p_info = pars[p_pos + 1].text
            infos = p_info.split('\n')
            time.sleep(0.2)
            for info in tqdm(infos, total=len(infos), desc="处理进度", ncols=100):
                segments = info.split(' ', 1)
                if len(segments) == 2:
                    first_part, second_part = segments
                    if trans_flag:
                        second_part = translate(second_part)
                    paragraph = output_file.add_paragraph(
                        f"{first_part} {second_part}")
                    paragraph_set(paragraph, default_paragraph_font, default_paragraph_font_size, 0, 8, 1.08, False,
                                  False)

    output_file_name = f"vCenter-{vcenter_ip}_巡检报告.docx"

    try:
        output_file.save(output_file_name)
    except Exception as error:
        print(f"保存过程出错: {error}")
        logging.error(f"保存过程出错: {error}")
        print(f"文件{output_file_name}可能被打开，尝试接管并关闭文件")
        logging.error(f"文件{output_file_name}可能被打开，尝试接管并关闭文件")
        try:
            word = ws.Dispatch('Word.Application')  # 打开word应用程序,并尝试自动关闭文档
            word.Visible = False
            word.DisplayAlerts = False
            doc = word.Documents.Open(f"{current_dir}\\{output_file_name}")
            doc.Close()
            word.Quit()
            time.sleep(1)  # 休眠1秒防止word程序未及时关闭导致出错，可根据实际运行状况增加
            output_file.save(output_file_name)
        except Exception as error_final:
            print(f"文件仍无法处理，进程结束\n错误详情: {error_final}")
            logging.error(f"文件仍无法处理，进程结束\n错误详情: {error_final}")
            return
    finally:
        global output_docx_name
        final_proc(f"{current_dir}\\{output_file_name}")
        output_docx_name = output_file_name


# 更新字典文件
def update_json():
    # 判断是否有未翻译的语句
    if translate_count:
        # 定义文件路径，这里假设字典文件位于当前目录下，文件名为"translate-dictionary.json"
        file_path = f"{current_dir}\\translate-dictionary.json"
        # 以写入模式打开文件，并指定编码为utf-8
        with open(file_path, "w", encoding='utf-8') as json_file:
            # 将字典数据转换为JSON格式，并写入文件
            # ensure_ascii=False表示允许输出非ASCII字符
            # indent=4表示缩进4个空格
            # separators=(',', ':')表示使用逗号和冒号作为分隔符
            json_file.write(json.dumps(trans_dict, ensure_ascii=False, indent=4, separators=(',', ':')))
        # 打印提示信息
        print(f"本次运行发现{translate_count}条未存在于字典文件中的语句，已将这些语句写入快速翻译字典文件中")
        # 记录日志信息
        logging.info(f"本次运行发现{translate_count}条未存在于字典文件中的语句，已将这些语句写入快速翻译字典文件中")


# 自动找到VHA原始报告文档，将文档处理为python-docx可读取的格式，如果已经有处理过的版本，则自动跳过
# 部分文档可能出现”文档损坏“问题导致无法处理，请手动打开问题文档并另存为，再交给本程序处理
# 该自动处理方法完全依赖VHA生成报告文档时的文件名特征，请勿做任何重命名操作以免程序找不到文档处理
@retry(tries=5, delay=3)
def rebuild_docx():
    source_list = []
    # rebuild_list = []
    for folder, folders, docx_files in os.walk(current_dir):
        for sourceFile in docx_files:
            # if fnmatch.fnmatch(sourceFile, 'vha-report-project-*.docx'):
            #     rebuild_flag = False
            #     for rebuild_file in docx_files:
            #         if rebuild_file == f'rebuild-{sourceFile}':
            #             # print(f"发现已经过处理的VHA原始报告文件: {rebuild_file} , 跳过对 {sourceFile} 的处理")
            #             logging.info(f"发现已经过处理的VHA原始报告文件: {rebuild_file} , 跳过对 {sourceFile} 的处理")
            #             rebuild_flag = True
            #             rebuild_list.append(os.path.join(folder, rebuild_file))
            #             break
            #     if rebuild_flag:
            #         continue
            #     else:
            #         source_list.append(os.path.join(folder, sourceFile))
            rebuild_flag = False
            if fnmatch.fnmatch(sourceFile, '*.docx'):
                if fnmatch.fnmatch(sourceFile, 'rebuild-*.docx'):
                    logging.info(
                        f"发现已经过处理的VHA原始报告文件: {sourceFile}")
                    # rebuild_list.append(os.path.join(folder, sourceFile))
                    continue
                elif not fnmatch.fnmatch(sourceFile, '~$*.docx') and sourceFile not in source_list:
                    check_doc = Document(os.path.join(folder, sourceFile))
                    for paragraph in check_doc.paragraphs:
                        if fnmatch.fnmatch(paragraph.text, '*VMware vSphere*Health Check Report*'):
                            source_list.append(os.path.join(folder, sourceFile))
                            rebuild_flag = True
                            break
            if rebuild_flag:
                continue

    # if len(rebuild_list) > 0:
        # print("找到以下已重建的VHA报告文件，无需处理:")
        # for i in rebuild_list:
        #     print(i)
    if len(source_list) > 0:
        print("找到以下VHA报告文件待重建，请稍等:")
        for i in source_list:
            print(i)
        for fn in tqdm(source_list, total=len(source_list), desc="重建进度", ncols=100):
            # print(f"发现VHA原始报告文件: {fn}")
            # logging.info(f"发现VHA原始报告文件: {fn}")
            try:
                word = ws.Dispatch('Word.Application')  # 打开word应用程序,DispatchEx为独立进程打开方法
                word.Visible = False
                word.DisplayAlerts = False
                doc = word.Documents.Open(fn)  # 打开word文件
                a = os.path.split(fn)  # 分离路径和文件
                b = os.path.splitext(a[-1])[0]  # 拿到文件名
                # print(f'正在预处理文档: {fn}')
                logging.debug(f'正在预处理文档: {fn}')
                doc.SaveAs(f"{a[0]}\\rebuild-{b}.docx", 12)
                # 另存为后缀为".docx"的文件，其中参数12或16指docx文件，python-docx只能读取参数为12的文件
                doc.Close()  # 关闭原来的word文件
                word.Quit()  # 关闭word程序
                if source_list.index(fn) < len(source_list) - 1:
                    # print(f'处理完成，文件已保存到: {a[0]}\\rebuild-{b}.docx，3秒后处理下一个')
                    logging.debug(f'处理完成，文件已保存到: {a[0]}\\rebuild-{b}.docx，3秒后处理下一个')
                    time.sleep(1)  # 程序等待1秒，确保word程序已关闭，防止报错
                else:
                    # print(f'处理完成，文件已保存到: {a[0]}\\rebuild-{b}.docx，全部处理完成')
                    logging.debug(f'处理完成，文件已保存到: {a[0]}\\rebuild-{b}.docx，全部处理完成')
            except Exception as err:
                # print(f'处理失败: {fn}')
                # print(f'失败原因: {err}')
                logging.error(f'处理失败: {fn}')
                logging.error(f'失败原因: {err}')


# 删除win32com的gen_py生成的临时文件，防止程序报错
def del_temp_file():
    user_folder = os.path.expanduser("~")
    # 构建要删除的目录路径
    gen_py_folder = os.path.join(user_folder, "AppData", "Local", "Temp", "gen_py")
    # 遍历目录，删除所有文件
    for root, dirs, _files in os.walk(gen_py_folder):
        for _filename in _files:
            file_path = os.path.join(root, _filename)
            try:
                os.unlink(file_path)
            except Exception as e:
                print(f"删除文件{file_path}时发生错误:{e}")
                logging.error(f"删除文件{file_path}时发生错误:{e}")
    # 删除空的子目录
    for root, dirs, _files in os.walk(gen_py_folder, topdown=False):
        for dir_name in dirs:
            dir_path = os.path.join(root, dir_name)
            try:
                os.rmdir(dir_path)
            except Exception as e:
                print(f"删除文件夹{dir_path}时发生错误:{e}")
                logging.error(f"删除文件夹{dir_path}时发生错误:{e}")


def resource_path(relative_path):
    # 判断当前程序是否为Bundle Resource，即是否被打包成可执行文件
    if getattr(sys, 'frozen', False):  # 是否Bundle Resource
        base_path = sys._MEIPASS  # 如果为Bundle Resource，则获取打包后的路径
    else:
        base_path = os.path.abspath(".")  # 如果非Bundle Resource，则获取当前工作目录的绝对路径
    return os.path.join(base_path, relative_path)  # 将基本路径和相对路径拼接后返回


def convert_seconds_to_minutes_seconds(seconds):
    # 使用divmod函数将秒数转换为分钟和余数
    minutes, remainder = divmod(int(seconds), 60)
    # 对余数进行四舍五入，保留三位小数
    seconds = round(remainder, 3)
    # 如果分钟数为0，则直接返回秒数的字符串形式
    if minutes == 0:
        return f"{seconds}秒"
    # 否则，返回分钟数和秒数的字符串形式
    else:
        return f"{minutes}分{seconds}秒"


if __name__ == '__main__':
    start_time = time.time()  # 计算程序运行开始时间
    last_time = time.time()
    read_config()  # 读取配置文件
    del_temp_file()  # 删除临时文件
    rebuild_docx()  # 重建文档
    dictionary()  # 读取字典文件

    for current_folder, list_folders, files in os.walk(current_dir):  # 寻找处理过的文档开始生成巡检报告
        for file in files:
            if fnmatch.fnmatch(file, 'rebuild-*.docx'):
                print(f"开始处理VHA报告文件: {file}")
                print(f"位于: {current_folder} 目录下")
                logging.info(f"开始处理VHA报告文件: {file}")
                logging.info(f"位于: {current_folder} 目录下")

                input_doc = Document(os.path.join(current_folder, file))
                input_doc_name = file
                if template_flag:
                    if template_flag_spec:
                        filename = resource_path(os.path.join("res", "vCenter_模板.docx"))
                        output_doc = Document(filename)
                        print(f"正在使用内置模板文件")
                        logging.debug(f"正在使用内置模板文件")
                    else:
                        output_doc = Document(template_filename)
                        print(f"正在使用外部模板文件: {template_filename}")
                        logging.debug(f"正在使用外部模板文件: {template_filename}")
                else:
                    print("未使用模板文件")
                    logging.debug("未使用模板文件")
                    output_doc = Document()
                analysis_data(input_doc, output_doc)
                file_time = time.time() - last_time
                last_time = time.time()
                print(
                    f"文件{output_docx_name}已处理完成并保存,处理该文件耗时{convert_seconds_to_minutes_seconds(file_time)}")
                logging.info(
                    f"文件{output_docx_name}已处理完成并保存,处理该文件耗时{convert_seconds_to_minutes_seconds(file_time)}")
    update_json()
    end_time = time.time()
    total_time = end_time - start_time
    # 计算用时
    print(f"共耗时: {convert_seconds_to_minutes_seconds(total_time)}")
    logging.info(f"共耗时: {convert_seconds_to_minutes_seconds(total_time)}")
    input('处理完成，输入回车结束')
